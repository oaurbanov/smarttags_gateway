/* ex: set tabstop=2 shiftwidth=2 expandtab cindent: */
#ifndef _UCODE_H
#define _UCODE_H
/**
 *  @file tm_reader.h
 *  @brief Mercury API Reader Interface
 *  @author Brian Fiegel
 *  @date 4/18/2009
 */

 /*
 * Copyright (c) 2009 ThingMagic, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "tmr_tag_protocol.h"
#include "tmr_gen2.h"


#ifdef  __cplusplus
extern "C" {
#endif

#define TMR_MAX_EPC_BYTE_COUNT (62) //62
#define TMR_MAX_TID_BYTE_COUNT (62)
#define TMR_MAX_USER_MEM_BYTE_COUNT (416)

#define DS_ATTR_LENGTH (32)
#define DS_DATA_LENGTH (203)
#define DS_LABEL_LENGTH (16)

//#define URL "https://osurval.upv.edu.es"
#define URL "https://smarttags.osurval.upv.edu.es"
#define URI "/php/registerMeasures.php"
#define GT_SERIAL "A1000001"//const char *GT_SERIAL = "A1000001";

/**
 * A structure to represent UCODE RFID tag.
 * @ingroup filter
 */
typedef struct UCODE_Tag
{
  /** Tag EPC */
  uint8_t epc[TMR_MAX_EPC_BYTE_COUNT];
  /** Tag TID */
  uint8_t tid[TMR_MAX_TID_BYTE_COUNT];
  /** Length of the tag's EPC in bytes */
  uint8_t epcByteCount;
  /** Length of the tag's TID in bytes */
  uint8_t tidByteCount;
  ///** Tag CRC */
  //uint16_t crc;

  /** Tag User memory */
  uint8_t userMem[TMR_MAX_USER_MEM_BYTE_COUNT];
  /** timestamp when userMem was aquired */
  long long timeStamp;

} UCODE_Tag;


/**
 * A structure to represent DataSet_tag obtained from the UCODE's userMem.
 */
typedef struct DataSet_tag
{
  uint8_t sensorType[DS_ATTR_LENGTH];//"temperature" , "humidity"
  uint8_t logType[DS_ATTR_LENGTH];//"cumulative" or "linear"
  uint8_t ttLog; //mins
  long long timeStamp; //DataSet timeStamp identifier
  uint16_t values[DS_DATA_LENGTH];
  uint8_t labels[DS_DATA_LENGTH][DS_LABEL_LENGTH];
  long long timeStamps[DS_DATA_LENGTH];

} DataSet_tag;


#ifdef  __cplusplus
}
#endif

#endif /* _TM_READER_H_ */
