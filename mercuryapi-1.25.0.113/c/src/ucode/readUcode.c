/**
 * Program that reads EPC, TID and User Memory bank data of UCODE RFID tags
 * organizes the info and send it to the main server.
 * @file readUcode.c
 */

#include <tm_reader.h>
#include <../ucode/ucode.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <inttypes.h>

#if WIN32
#define snprintf sprintf_s
#endif

/* Enable this to use transportListener */
#ifndef USE_TRANSPORT_LISTENER
#define USE_TRANSPORT_LISTENER 0
#endif

#define usage() {errx(1, "Please provide reader URL, such as:\n"\
                         "tmr:///com4 or tmr:///com4 --ant 1,2\n"\
                         "tmr://my-reader.example.com or tmr://my-reader.example.com --ant 1,2\n");}




/*To measure response time*/
typedef long long timeStamp_t;
static timeStamp_t getTimeStamp()
{
    struct timeval now;
    gettimeofday(&now, NULL);
    return now.tv_sec;
}

void generateDataSet(UCODE_Tag dataLogger, DataSet_tag *dataSet, int v){

  //UCODE_Tag dataLogger;
  //dataLogger = *dataLogger2;
  //DataSet_tag dataSet;
  //dataSet = *dataSet2;

  if (v) printf("\n\n ------ Generating DataSet ------ \n\n");

  uint8_t configByte = 0x00;
  configByte = dataLogger.userMem[0];
  if(v) printf("configByte detected: %d\n", configByte);


  //ttLog extraction and clasifying
  uint8_t auxTtLog = configByte & 0x03;
  uint8_t ttLog = 0x00;
  if (auxTtLog == 0x03) ttLog = 60;
  else if (auxTtLog == 0x02) ttLog = 30;
  else if (auxTtLog == 0x01) ttLog = 10;
  else if (auxTtLog == 0x00) ttLog = 5;
  dataSet->ttLog = ttLog;
  if(v) printf("ttLog detected: %d\n", dataSet->ttLog);

  //sensorType extraction and clasifying
  uint8_t auxSensorType = (configByte & 0x7C)>>2;// 7C = 01111100
  uint8_t *sensorType;
  if (auxSensorType == 0x00) sensorType = "temperature";
  else if (auxSensorType == 0x01) sensorType = "humidity";
  else if (auxSensorType == 0x02) sensorType = "vibration";
  else sensorType = "none";
  memcpy(dataSet->sensorType, sensorType, DS_ATTR_LENGTH );
  if(v) printf("sensorType detected: %s\n", dataSet->sensorType);

  //logType setting
  uint8_t *logType = "cumulative";
  memcpy(dataSet->logType, logType, DS_ATTR_LENGTH);
  if (v) printf("logType: %s\n", dataSet->logType);

  dataSet->timeStamp = getTimeStamp();
  char timeStampStr[100];
  date2send(timeStampStr , dataSet->timeStamp);
  if (v) printf("timeStamp: %s\n", timeStampStr );

  //userMem Data extraction
  memcpy(dataSet->values, dataLogger.userMem+1, sizeof(dataSet->values));//TODO: 1 byte shift?, switch penultimo value at the beginning
  if(v){
    printf("DataSet values: \n");
    int i;
    for (i = 0; i < sizeof(dataSet->values)/sizeof(uint16_t) ; i++) //203
    {
      printf(" %i", dataSet->values[i] );
    }
    printf("\n");    
  }

  //labels Data creation
  uint8_t label[DS_LABEL_LENGTH];
  float val = 0, ini = -10, delta = 0.2;
  int i, count;
  count = sizeof(dataSet->values)/sizeof(uint16_t);//203
  for (i = 0; i < count ; i++)
  {
    if (i==0){
      sprintf( label, "<%.1f", ini );
    }else if (i==count-1){
      sprintf( label, ">%.1f", val );
    }else{
      val = ini + delta*(i-1);
      sprintf( label, "%.1f", val );
    }
    strcpy(dataSet->labels[i],label);
  }
  //labels Data printing
  if (v) {
    printf("DataSet labels:\n");
    for (i = 0; i < count ; i++)
    {
      printf("%s ", dataSet->labels[i]);
    }    
  }

}
void printDataSetInfo(DataSet_tag dataSet){
  printf("\n\n\n ----- DataSet Info:----- \n\n\n");
  printf("sensorType: %s\n", dataSet.sensorType);
  printf("logType %s\n", dataSet.logType);
  printf("ttLog: %d\n", dataSet.ttLog);

  char timeStampStr[100];
  date2send(timeStampStr, dataSet.timeStamp);
  printf(" My timeStamp: %s\n", timeStampStr);



    printf("Values: \n");
    int i;
    for (i = 0; i < sizeof(dataSet.values)/sizeof(uint16_t) ; i++)
    {
      printf("%i) temperature: %s : %i \n", i, dataSet.labels[i], dataSet.values[i] );
    }
    printf("\n");    

}

void printTagInfo(UCODE_Tag *dataLogger2){
  printf("\n\n  ----- UCODE_tag Info: ----- \n\n");
  
  UCODE_Tag dataLogger;
  dataLogger = *dataLogger2;

  char dataStr[258];
  TMR_bytesToHex(dataLogger.epc, dataLogger.epcByteCount, dataStr);
  printf(" My EPC data(%d): %s\n", dataLogger.epcByteCount, dataStr);

  //char dataStr[258];
  TMR_bytesToHex(dataLogger.tid, dataLogger.tidByteCount, dataStr);
  printf(" My TID data(%d): %s\n", dataLogger.tidByteCount, dataStr);

  char dataStr2[416];
  TMR_bytesToHex(dataLogger.userMem, 416, dataStr2);
  printf(" My userMem data(%d): %s\n", 416, dataStr2);

  //dataLogger.timeStamp = getTimeStamp();
  char tagTimeStampStr[100];
  date2send(tagTimeStampStr, dataLogger.timeStamp);
  printf(" My timeStamp: %s\n", tagTimeStampStr);

}

void printNetUCODEData(uint8_t *UserMem){

  uint8_t info [416];
  memcpy(info,UserMem,416);

  //if info[] is printed it is also modified
  //char dataStr2[416];
  //TMR_bytesToHex(info, 416, dataStr2);
  //printf(" \n  This is DTH22 userMem data(%d): %s\n", 416, dataStr2);


  int infoSize = 416;
  int measureSize = 6;
  int totalMeasures = infoSize/measureSize;

  uint8_t dht22_dat[6];
  float t, h;
  int k=0; int z=0; int y=0;


  y=0;
  for(z=0;z<totalMeasures;z++){//for totalMeasures
      for (k=0;k<6;k++){
          dht22_dat[5-k] = info[y];
          y++;
      }
      
      printf("%i_ Info: ",z);
      for(k=0;k<6;k++){
          printf("%02hhX",dht22_dat[k]);
      }
      printf("\n");

      if (dht22_dat[4] == ((dht22_dat[0] + dht22_dat[1] + dht22_dat[2] + dht22_dat[3])& 0xFF) ){
          printf("******* PARITY CHECK OK *******\n");
      }else{
          //printf("------- BAD PARITY CHECK ------\n");
      }

      printf("Parity Byte: %i  ",dht22_dat[4]);
      printf("Sum: %i\n", (dht22_dat[0] + dht22_dat[1] + dht22_dat[2] + dht22_dat[3])& 0xFF );

      t=0;h=0;
      h = (float)dht22_dat[0] * 256 + (float)dht22_dat[1];
      h /= 10;
      t = (float)(dht22_dat[2] & 0x7F)* 256 + (float)dht22_dat[3];
      t /= 10.0;
      if ((dht22_dat[2] & 0x80) != 0)  t *= -1;
      printf("Humidity = %.2f %% Temperature = %.2f *C \n\n", h, t );
      
  } //for totalMeasures



}


int date2send(char *tagTimeStampStr, long int tagTimeStamp) {
/*
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
*/
    time_t rawtime = tagTimeStamp;
    struct tm * ptm;
    time(&rawtime);
    ptm = gmtime(&rawtime);
    sprintf(tagTimeStampStr, "%04d-%02d-%02d %02d:%02d:%02d\0", 
            ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday, ptm->tm_hour+1, ptm->tm_min, ptm->tm_sec);
    //sprintf(tagTimeStampStr, "%04d%02d%02dT%02d%02d%02d000Z\0", ptm->tm_year + 1900, ptm->tm_mon + 1, ptm->tm_mday, ptm->tm_hour, ptm->tm_min, ptm->tm_sec);
}

void createPayload(char *payload, UCODE_Tag *dataLogger, DataSet_tag *dataSet, int v){

  char *tag_tid = "04FF01234567890123456789"; //TODO leave real tag TID
  //char tag_tid[128];
  //TMR_bytesToHex( dataLogger->tid, dataLogger->tidByteCount, tag_tid );
  if (v) printf("tag tid: %s\n", tag_tid);

  char *tag_epc = "04EE01234567890123456789"; //TODO leave real tag EPC
  //char tag_epc[128];
  //TMR_bytesToHex( dataLogger->epc, dataLogger->epcByteCount, tag_epc );
  if (v) printf("tag epc: %s\n", tag_epc);

  char tag_timeStamp[64];
  date2send(tag_timeStamp, dataLogger->timeStamp);
  if (v) printf("tag timeStamp: %s\n", tag_timeStamp);

  char ds_timeStamp[64];
  date2send(ds_timeStamp, dataSet->timeStamp);
  if (v) printf("dataSet timeStamp: %s\n", ds_timeStamp);

  //creating values array
  char ds_values [1024];
  int i, count;
  strcpy(ds_values, "[");
  char ival[16];
  count = sizeof(dataSet->values)/sizeof(uint16_t);
  for (i = 0; i < count; i++){
    if(i==count-1){
      sprintf(ival, " %i ", dataSet->values[i]);
      strcat(ds_values, ival);
    }else{
      sprintf(ival, " %i ,", dataSet->values[i]);
      strcat(ds_values, ival);      
    }
  }
  strcat(ds_values, "]");
  if (v) printf("data Set values: %s\n", ds_values);

  //creating labels array
  char ds_labels [2048];
  //int i, count;
  strcpy(ds_labels, "[");
  count = sizeof(dataSet->values)/sizeof(uint16_t);
  for (i = 0; i < count; i++){
    if(i==count-1){
      strcat(ds_labels, "\"");
      strcat(ds_labels, dataSet->labels[i]);
      strcat(ds_labels, "\"");
    }else{
      strcat(ds_labels, "\"");
      strcat(ds_labels, dataSet->labels[i]);      
      strcat(ds_labels, "\"");
      strcat(ds_labels, " , ");      
    }
  }
  strcat(ds_labels, "]");
  if (v) printf("data Set labels: %s\n", ds_labels);

  //TODO: creating timeStamps array
  char *ds_timeStamps = "[ ]";

  sprintf(payload,"{  \"Gateway\": {  \"gatewaySerial\": \"%s\"   },  \"Tag\" : {  \"TID\":\"%s\" ,    \"EPC\":\"%s\",  \"timeStamp\": \"%s\" },  \"DataSet\": {  \"sensorType\" : \"%s\",    \"logType\" : \"%s\", \"ttLog\" : %i,  \"timeStamp\" : \"%s\",  \"values\" : %s,  \"labels\" : %s,  \"timeStamps\" : %s }   }", GT_SERIAL, tag_tid, tag_epc, tag_timeStamp, dataSet->sensorType , dataSet->logType , dataSet->ttLog , ds_timeStamp, ds_values, ds_labels, ds_timeStamps );
}

void send2Server(UCODE_Tag dataLogger, DataSet_tag dataSet, int v){
  if (v) printf("\n\n\n ----- Sending to IoT Server:----- \n\n\n");

  char payload[10240];
  createPayload(payload , &dataLogger, &dataSet, v);
  if (v) printf("\n payload:---> \n %s\n", payload);

  char curlCommand[10240];
  sprintf(curlCommand,
   "curl -X POST --header \"Content-Type: application/json\" -d  \' %s \' -v %s%s  ", payload ,URL, URI);
  
  if (v) printf("\ncurlCommand:-->\n %s\n", curlCommand); fflush(stdout);
  system(curlCommand); fflush(stdout);
}


void errx(int exitval, const char *fmt, ...)
{
  va_list ap;

  va_start(ap, fmt);
  vfprintf(stderr, fmt, ap);

  exit(exitval);
}

void checkerr(TMR_Reader* rp, TMR_Status ret, int exitval, const char *msg)
{
  if (TMR_SUCCESS != ret)
  {
    errx(exitval, "Error %s: %s\n", msg, TMR_strerr(rp, ret));
  }
}

void serialPrinter(bool tx, uint32_t dataLen, const uint8_t data[],
                   uint32_t timeout, void *cookie)
{
  FILE *out = cookie;
  uint32_t i;

  fprintf(out, "%s", tx ? "Sending: " : "Received:");
  for (i = 0; i < dataLen; i++)
  {
    if (i > 0 && (i & 15) == 0)
    {
      fprintf(out, "\n         ");
    }
    fprintf(out, " %02x", data[i]);
  }
  fprintf(out, "\n");
}

void stringPrinter(bool tx,uint32_t dataLen, const uint8_t data[],uint32_t timeout, void *cookie)
{
  FILE *out = cookie;

  fprintf(out, "%s", tx ? "Sending: " : "Received:");
  fprintf(out, "%s\n", data);
}

void parseAntennaList(uint8_t *antenna, uint8_t *antennaCount, char *args)
{
  char *token = NULL;
  char *str = ",";
  uint8_t i = 0x00;
  int scans;

  /* get the first token */
  if (NULL == args)
  {
    fprintf(stdout, "Missing argument\n");
    usage();
  }

  token = strtok(args, str);
  if (NULL == token)
  {
    fprintf(stdout, "Missing argument after %s\n", args);
    usage();
  }

  while(NULL != token)
  {
    scans = sscanf(token, "%"SCNu8, &antenna[i]);
    if (1 != scans)
    {
      fprintf(stdout, "Can't parse '%s' as an 8-bit unsigned integer value\n", token);
      usage();
    }
    i++;
    token = strtok(NULL, str);
  }
  *antennaCount = i;
}

void readAllMemBanks(TMR_Reader *rp, uint8_t antennaCount, uint8_t *antennaList, TMR_TagOp *op, TMR_TagFilter *filter, uint8_t *Data)
{
  TMR_ReadPlan plan;
  uint8_t data[258];
  TMR_Status ret;
  TMR_uint8List dataList;
  dataList.len = dataList.max = 258;
  dataList.list = data;

  TMR_RP_init_simple(&plan, antennaCount, antennaList, TMR_TAG_PROTOCOL_GEN2, 1000);

  ret = TMR_RP_set_filter(&plan, filter);
  checkerr(rp, ret, 1, "setting tag filter");

  ret = TMR_RP_set_tagop(&plan, op);
  checkerr(rp, ret, 1, "setting tagop");

  /* Commit read plan */
  ret = TMR_paramSet(rp, TMR_PARAM_READ_PLAN, &plan);
  checkerr(rp, ret, 1, "setting read plan");

  ret = TMR_read(rp, 500, NULL);
  if (TMR_ERROR_TAG_ID_BUFFER_FULL == ret)
  {
    /* In case of TAG ID Buffer Full, extract the tags present
    * in buffer.
    */
    fprintf(stdout, "reading tags:%s\n", TMR_strerr(rp, ret));
  }
  else
  {
    checkerr(rp, ret, 1, "reading tags");
  }

  while (TMR_SUCCESS == TMR_hasMoreTags(rp))
  {
    TMR_TagReadData trd;
    uint8_t dataBuf[258];
    uint8_t dataBuf1[258];
    uint8_t dataBuf2[258];
    uint8_t dataBuf3[258];
    uint8_t dataBuf4[258];
    char epcStr[128];

    ret = TMR_TRD_init_data(&trd, sizeof(dataBuf)/sizeof(uint8_t), dataBuf);
    checkerr(rp, ret, 1, "creating tag read data");

    trd.userMemData.list = dataBuf1;
    trd.epcMemData.list = dataBuf2;
    trd.reservedMemData.list = dataBuf3;
    trd.tidMemData.list = dataBuf4;

    trd.userMemData.max = 258;
    trd.userMemData.len = 0;
    trd.epcMemData.max = 258;
    trd.epcMemData.len = 0;
    trd.reservedMemData.max = 258;
    trd.reservedMemData.len = 0;
    trd.tidMemData.max = 258;
    trd.tidMemData.len = 0;

    ret = TMR_getNextTag(rp, &trd);
    checkerr(rp, ret, 1, "fetching tag");

    TMR_bytesToHex(trd.tag.epc, trd.tag.epcByteCount, epcStr);
    printf("%s\n", epcStr);
    //osc: this part is unnecesary, also it's just printing data
    // if (0 < trd.data.len)
    // {
    //   char dataStr[258];
    //   TMR_bytesToHex(trd.data.list, trd.data.len, dataStr);
    //   printf("  data(%d): %s\n", trd.data.len, dataStr);
    // }
    // if (0 < trd.userMemData.len)
    // {
    //   char dataStr[258];
    //   TMR_bytesToHex(trd.userMemData.list, trd.userMemData.len, dataStr);
    //   printf("  userData(%d): %s\n", trd.userMemData.len, dataStr);
    // }
    // if (0 < trd.epcMemData.len)
    // {
    //   char dataStr[258];
    //   TMR_bytesToHex(trd.epcMemData.list, trd.epcMemData.len, dataStr);
    //   printf(" epcData(%d): %s\n", trd.epcMemData.len, dataStr);
    // }
    // if (0 < trd.reservedMemData.len)
    // {
    //   char dataStr[258];
    //   TMR_bytesToHex(trd.reservedMemData.list, trd.reservedMemData.len, dataStr);
    //   printf("  reservedData(%d): %s\n", trd.reservedMemData.len, dataStr);
    // }
    // if (0 < trd.tidMemData.len)
    // {
    //   char dataStr[258];
    //   TMR_bytesToHex(trd.tidMemData.list, trd.tidMemData.len, dataStr);
    //   printf("  tidData(%d): %s\n", trd.tidMemData.len, dataStr);
    // }
  }

  ret = TMR_executeTagOp(rp, op, filter,&dataList);
  checkerr(rp, ret, 1, "executing the read all mem bank");
  if (0 < dataList.len)
  {
    char dataStr[258];
    TMR_bytesToHex(dataList.list, dataList.len, dataStr);
    printf("  Data(%d): %s\n", dataList.len, dataStr);

    // -- here send back info data (full EPC or TID) with the corresponding EPC--//
    
    memcpy(Data,dataList.list, dataList.len);

    // --------------------------------------------------------------------------//

  }
}


int main(int argc, char *argv[])
{
  TMR_Reader r, *rp;
  TMR_Status ret;
  TMR_Region region;
  TMR_ReadPlan plan;
  TMR_TagFilter filter;
  TMR_TagReadData trd;
  uint8_t *antennaList = NULL;
  uint8_t buffer[20];
  char epcString[128];
  uint8_t i;
  uint8_t antennaCount = 0x0;
  TMR_String model;
  char str[64];
#if USE_TRANSPORT_LISTENER
  TMR_TransportListenerBlock tb;
#endif

  if (argc < 2)
  {
    usage();
  }
  
  for (i = 2; i < argc; i+=2)
  {
    if(0x00 == strcmp("--ant", argv[i]))
    {
      if (NULL != antennaList)
      {
        fprintf(stdout, "Duplicate argument: --ant specified more than once\n");
        usage();
      }
      parseAntennaList(buffer, &antennaCount, argv[i+1]);
      antennaList = buffer;
    }
    else
    {
      fprintf(stdout, "Argument %s is not recognized\n", argv[i]);
      usage();
    }
  }

  rp = &r;
  ret = TMR_create(rp, argv[1]);
  checkerr(rp, ret, 1, "creating reader");

#if USE_TRANSPORT_LISTENER

  if (TMR_READER_TYPE_SERIAL == rp->readerType)
  {
    tb.listener = serialPrinter;
  }
  else
  {
    tb.listener = stringPrinter;
  }
  tb.cookie = stdout;

  TMR_addTransportListener(rp, &tb);
#endif

  ret = TMR_connect(rp);
  checkerr(rp, ret, 1, "connecting reader");

  region = TMR_REGION_NONE;
  ret = TMR_paramGet(rp, TMR_PARAM_REGION_ID, &region);
  checkerr(rp, ret, 1, "getting region");

  if (TMR_REGION_NONE == region)
  {
    TMR_RegionList regions;
    TMR_Region _regionStore[32];
    regions.list = _regionStore;
    regions.max = sizeof(_regionStore)/sizeof(_regionStore[0]);
    regions.len = 0;

    ret = TMR_paramGet(rp, TMR_PARAM_REGION_SUPPORTEDREGIONS, &regions);
    checkerr(rp, ret, __LINE__, "getting supported regions");

    if (regions.len < 1)
    {
      checkerr(rp, TMR_ERROR_INVALID_REGION, __LINE__, "Reader doesn't supportany regions");
    }
    region = regions.list[0];
    ret = TMR_paramSet(rp, TMR_PARAM_REGION_ID, &region);
    checkerr(rp, ret, 1, "setting region");  
  }

  model.value = str;
  model.max = 64;
  TMR_paramGet(rp, TMR_PARAM_VERSION_MODEL, &model);
  if (((0 == strcmp("M6e Micro", model.value)) ||(0 == strcmp("M6e Nano", model.value)))
    && (NULL == antennaList))
  {
    fprintf(stdout, "Module doesn't has antenna detection support please provide antenna list\n");
    usage();
  }

  /**
  * for antenna configuration we need two parameters
  * 1. antennaCount : specifies the no of antennas should
  *    be included in the read plan, out of the provided antenna list.
  * 2. antennaList  : specifies  a list of antennas for the read plan.
  **/ 

  // initialize the read plan 
  ret = TMR_RP_init_simple(&plan, antennaCount, antennaList, TMR_TAG_PROTOCOL_GEN2, 1000);
  checkerr(rp, ret, 1, "initializing the  read plan");

  /* Commit read plan */
  ret = TMR_paramSet(rp, TMR_PARAM_READ_PLAN, &plan);
  checkerr(rp, ret, 1, "setting read plan");

  //Use first antenna for tag operation
  if (NULL != antennaList)
  {
    ret = TMR_paramSet(rp, TMR_PARAM_TAGOP_ANTENNA, &antennaList[0]);
    checkerr(rp, ret, 1, "setting tagop antenna");  
  }

  ret = TMR_read(rp, 500, NULL);
  if (TMR_ERROR_TAG_ID_BUFFER_FULL == ret)
  {
    /* In case of TAG ID Buffer Full, extract the tags present
    * in buffer.
    */
    fprintf(stdout, "reading tags:%s\n", TMR_strerr(rp, ret));
  }
  else
  {
    checkerr(rp, ret, 1, "reading tags");
  }

  if (TMR_ERROR_NO_TAGS == TMR_hasMoreTags(rp))
  {
    errx(1, "No tags found for test\n");
  }

  ret = TMR_getNextTag(rp, &trd);
  checkerr(rp, ret, 1, "getting tags");

  TMR_TF_init_tag(&filter, &trd.tag);
  TMR_bytesToHex(filter.u.tagData.epc, filter.u.tagData.epcByteCount,
    epcString);

  {
    TMR_TagOp tagop;
    TMR_uint16List writeData;
    TMR_TagData epc;
    uint8_t readLength = 0x00;
    uint16_t data[] = {0x1234, 0x5678};
    uint16_t data1[] = {0xFFF1, 0x1122};
    uint8_t epcData[] = {
      0x01, 0x23, 0x45, 0x67, 0x89, 0xAB,
      0xCD, 0xEF, 0x01, 0x23, 0x45, 0x67,
    };

    if ((0 == strcmp("M6e", model.value)) || (0 == strcmp("M6e PRC", model.value))
      || (0 == strcmp("M6e Micro", model.value)) || (0 == strcmp("Mercury6", model.value)) 
      || (0 == strcmp("Astra-EX", model.value)))
    {
      /**
      * Specifying the readLength = 0 will retutrn full Memory bank data for any
      * tag read in case of M6e  and its varients and M6 reader.
      **/ 
      readLength = 0;
    }
    else
    {
      /**
      * In other case readLen is minimum.i.e 2 words
      **/
      readLength = 2;
    }

    // /* write Data on EPC bank */
    // epc.epcByteCount = sizeof(epcData) / sizeof(epcData[0]);
    // memcpy(epc.epc, epcData, epc.epcByteCount * sizeof(uint8_t));
    // ret = TMR_TagOp_init_GEN2_WriteTag(&tagop, &epc);
    // checkerr(rp, ret, 1, "initializing GEN2_WriteTag");
    // ret = TMR_executeTagOp(rp, &tagop, NULL, NULL);
    // checkerr(rp, ret, 1, "executing the write tag operation");
    // printf("Writing on EPC bank success \n");

   //  /* Write Data on reserved bank */
   //  writeData.list = data;
   //  writeData.max = writeData.len = sizeof(data) / sizeof(data[0]);
   //  ret = TMR_TagOp_init_GEN2_BlockWrite(&tagop, TMR_GEN2_BANK_RESERVED, 0, &writeData);
   //  checkerr(rp, ret, 1, "Initializing the write operation");
   //  ret = TMR_executeTagOp(rp, &tagop, NULL, NULL);
   //  checkerr(rp, ret, 1, "executing the write operation");
   //  printf("Writing on RESERVED bank success \n");

   //  /* Write data on user bank */
   //  writeData.list = data1;
   //  writeData.max = writeData.len = sizeof(data1) / sizeof(data1[0]);
   //  ret = TMR_TagOp_init_GEN2_BlockWrite(&tagop, TMR_GEN2_BANK_USER, 0, &writeData);
   //  checkerr(rp, ret, 1, "Initializing the write operation");
	  // ret = TMR_executeTagOp(rp, &tagop, NULL, NULL);
   //  checkerr(rp, ret, 1, "executing the write operation");
   //  printf("Writing on USER bank success \n");



    struct UCODE_Tag dataLogger;
    uint8_t Data[258];


    printf(" Perform embedded and standalone tag operation - read epc memory without filter\n");
    ret = TMR_TagOp_init_GEN2_ReadData(&tagop, (TMR_GEN2_BANK_EPC), 0, readLength);
    readAllMemBanks(rp, antennaCount, antennaList, &tagop, NULL, Data);
    memcpy(dataLogger.epc, Data, 16); 
    dataLogger.epcByteCount = 16;


    printf(" Perform embedded and standalone tag operation - read tid memory without filter\n");
    ret = TMR_TagOp_init_GEN2_ReadData(&tagop, (TMR_GEN2_BANK_TID), 0, readLength);
    readAllMemBanks(rp, antennaCount, antennaList, &tagop, NULL, Data);
    memcpy(dataLogger.tid, Data, 32);
    dataLogger.tidByteCount = 32;



    //--------------This section reads all the User memory from the UCODE 

    //osc: Because a hundred words is the maximum amount that can be read,
    // data is readed (before causing timeout) in banks of a hundred words and the last 8 words
    // 208 * 2 = 416 total User Memory

    readLength = 100;// //osc: length in words, 100 is the maximum accepted
    printf("Perform embedded and standalone tag operation - read only user memory without filter \n");
    ret = TMR_TagOp_init_GEN2_ReadData(&tagop, (TMR_GEN2_BANK_USER), 0, readLength);
    readAllMemBanks(rp, antennaCount, antennaList, &tagop, NULL, Data);
    memcpy(dataLogger.userMem, Data, 200);

    readLength = 100;
    printf("Perform embedded and standalone tag operation - read only user memory without filter \n");
    ret = TMR_TagOp_init_GEN2_ReadData(&tagop, (TMR_GEN2_BANK_USER), 100, readLength);
    readAllMemBanks(rp, antennaCount, antennaList, &tagop, NULL, Data);
    memcpy(dataLogger.userMem+200, Data, 200);

    readLength = 8;
    printf("Perform embedded and standalone tag operation - read only user memory without filter \n");
    ret = TMR_TagOp_init_GEN2_ReadData(&tagop, (TMR_GEN2_BANK_USER), 200, readLength);
    readAllMemBanks(rp, antennaCount, antennaList, &tagop, NULL, Data);
    memcpy(dataLogger.userMem+400, Data, 16);

    //--------------This section reads all the User memory from the UCODE 

    dataLogger.timeStamp = getTimeStamp();
    printTagInfo(&dataLogger);
    //printNetUCODEData(dataLogger.userMem);//includes paritycheck
    


    struct DataSet_tag dataSet;
  
    generateDataSet(dataLogger, &dataSet, 1);
    printDataSetInfo(dataSet);

    send2Server(dataLogger, dataSet, 1);






    
    //osc: the concatenation TMR_GEN2_BANK_USER | TMR_GEN2_BANK_EPC_ENABLED is not working
    // every bank memory has to be read separately

    //printf("Perform embedded and standalone tag operation - read all memory bank without filter \n");
    //ret = TMR_TagOp_init_GEN2_ReadData(&tagop, (TMR_GEN2_BANK_USER | TMR_GEN2_BANK_EPC_ENABLED | TMR_GEN2_BANK_RESERVED_ENABLED |TMR_GEN2_BANK_TID_ENABLED |TMR_GEN2_BANK_USER_ENABLED), 0, readLength);
    //readAllMemBanks(rp, antennaCount, antennaList, &tagop, NULL);

  }

  TMR_destroy(rp);
  return 0;
}

