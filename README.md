# README #

This repo contains the source code for the gateway of the project SmartTags. Made for the BeagleBoneBlack running on BeagleBoardDebian jessie version.

### How do I get set up? ###

Go to the dir and compile:
$cd /mercuryapi-1.25.0.113/c/src/api
$make

There you have the executables.

Compilation steps are in README of the mercury API.


### Who do I talk to? ###

Oscar Urbano 	oaurbanov@unal.edu.co

